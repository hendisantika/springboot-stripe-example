package com.hendisantika.springbootstripeexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootStripeExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootStripeExampleApplication.class, args);
    }

}
